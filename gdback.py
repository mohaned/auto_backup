import os
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

MOKATTAM_ID = '0B-L4KbN1f0ELallfYlRGT0ZHaEk'
ZETA_ID = '0B-L4KbN1f0ELSHJEc0VZM2QyaGs'
MANARAT_ID = '0B-L4KbN1f0ELRUxIZjVLZUZQcFE'


def login(settings_path=''):
    global gauth, drive
    gauth = GoogleAuth(settings_path)

    if gauth.credentials is None:
        gauth.LocalWebserverAuth()
    elif gauth.access_token_expired:
        gauth.Refresh()
    else:
        gauth.Authorize()

    drive = GoogleDrive(gauth)


def upload_files_to_folder(fnames, folder=None, folder_id=None):
    for fname in fnames:
        nfile = drive.CreateFile({'title': os.path.basename(fname),
                                  'parents': [{u'id': folder_id if folder_id else folder['id']}]})
        nfile.SetContentFile(fname)
        nfile.Upload()


def upload_backup(backup_path, folder_id, settings_path):
    login(settings_path)
    upload_files_to_folder([backup_path], folder_id=folder_id)
